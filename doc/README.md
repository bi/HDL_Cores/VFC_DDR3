# VFC-HD DDR3 Controller IP Core

This project contains two examples of using DDR3 memories on board of VFC-HD v3. The examples are in form of a complete Altera Quartus Prime projects.

The `ddr_example` project shows usage of two independent soft cores for the DDRs. One core is connected to custom written memtest utility, that uses the Avalon-MM burst enabled buses to demonstrate fast access to the RAM. The second core is connected to a simple data dumper, which dumps a ramping pattern into the RAM. The dumped data are available to read via VME bus. This demonstrates a simple usage example of a system which dumps data into memory to be read out later, for `xpoc` for example. The cores operate at 533.33 MHz. The front panel LEDs are used to signalize success of the DDR operation. All LEDs should come ON some seconds after programming the VFC card. The VME bus readout should give a ramping patter. There are two 16-bit words in each 32-bit VME readout. The VME driver uses geographic addressing. IP core presets for the case similar to this one can be found in files [VFC-Master.qprs](../VFC-Master.qprs) and [VFC-Slave.qprs](../VFC-Slave.qprs) (having the same settings as described, but sharing DLL, PLL and OCT parts).

The `ddr_core_manual` project shows usage of a single soft core at 666.66 MHz frequency. EMIF is enabled to allow debugging. The core is connected to an example design tester generated by the IP generator. The front panel LEDs are used to signalize success of the DDR operation. All except LEDs 1 and 5 should come ON some seconds after programming the VFC card. One can switch from DDR A to DDR B by simply rewriting the project QSF file with one of the two supplied alternatives.

Read the [vfcddr.pdf](vfcddr.pdf) for more information and detail on working with the DDRs on VFC.

:exclamation: There are three typos in the above mentioned PDF file. The template to be selected (Figure 2.1) should be "MICRON MT41K256M16HA-125" and correct values in the "Memory Parameters" -> "Memory Initialization Options" (Figure 2.2) should be as follows:

 - Mode Register 1: Output drive strength setting: RZQ/6
 - Mode Register 2: Dynamic ODT (Rtt_WR) value: RZQ/4