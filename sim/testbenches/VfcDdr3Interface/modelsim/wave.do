onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_VfcDdr3Interface/DdrInitMemory_p
add wave -noupdate -divider {Std Interface}
add wave -noupdate /tb_VfcDdr3Interface/i_DUT/StdClk_ik
add wave -noupdate -radix unsigned /tb_VfcDdr3Interface/i_DUT/StdAddress_ib26
add wave -noupdate /tb_VfcDdr3Interface/i_DUT/StdByteWe_ib16
add wave -noupdate -radix unsigned /tb_VfcDdr3Interface/i_DUT/StdData_ib128
add wave -noupdate /tb_VfcDdr3Interface/i_DUT/StdWriteReady_o
add wave -noupdate -divider {Avalon Bus}
add wave -noupdate -color Yellow /tb_VfcDdr3Interface/AvlClk_k
add wave -noupdate -color Cyan /tb_VfcDdr3Interface/AvlRead
add wave -noupdate -color Cyan /tb_VfcDdr3Interface/AvlWrite
add wave -noupdate -color Cyan /tb_VfcDdr3Interface/AvlBurstCount_b11
add wave -noupdate -color Cyan /tb_VfcDdr3Interface/AvlByteEnable_b16
add wave -noupdate -color Yellow /tb_VfcDdr3Interface/AvlWaitRequest
add wave -noupdate -color Cyan -radix unsigned /tb_VfcDdr3Interface/AvlAddress_b26
add wave -noupdate -color Cyan -radix unsigned /tb_VfcDdr3Interface/AvlDataWrite_b128
add wave -noupdate -color Yellow -radix unsigned /tb_VfcDdr3Interface/AvlDataRead_b128
add wave -noupdate -color Yellow /tb_VfcDdr3Interface/AvlReadDataValid
add wave -noupdate /tb_VfcDdr3Interface/i_DUT/AvlState_l
add wave -noupdate /tb_VfcDdr3Interface/i_DUT/StdWriteFifoEmpty
add wave -noupdate /tb_VfcDdr3Interface/i_DUT/FifoOutMemDav_b
add wave -noupdate /tb_VfcDdr3Interface/i_DUT/ReStdWriteFifo
add wave -noupdate -divider WishBone
add wave -noupdate /tb_VfcDdr3Interface/WbWe
add wave -noupdate /tb_VfcDdr3Interface/WbStb
add wave -noupdate /tb_VfcDdr3Interface/WbRst
add wave -noupdate /tb_VfcDdr3Interface/WbDatMoSi_b32
add wave -noupdate /tb_VfcDdr3Interface/WbDatMiso_b32
add wave -noupdate /tb_VfcDdr3Interface/WbCyc
add wave -noupdate /tb_VfcDdr3Interface/WbClk_k
add wave -noupdate /tb_VfcDdr3Interface/WbAdr_b32
add wave -noupdate /tb_VfcDdr3Interface/WbAck
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {17317200 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 222
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {17240560 ps} {18152790 ps}
