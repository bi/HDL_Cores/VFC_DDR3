.main clear

vlog -work work ../../../../VfcDdr3Interface.sv
vlog -work work ../../../models/generic_dpram_mod.v
vlog -work work ../../../models/generic_fifo_dc_gray_mod.v
vlog -work work ../../../models/WbMasterSim.sv
vlog -work work ../tb_VfcDdr3Interface.sv
