`timescale 1ns/10ps

module tb_VfcDdr3Interface ();

logic WbClk_k = 0;
always #2.5 WbClk_k = ~WbClk_k; //200MHz
logic AvlClk_k = 0;
always #3.75 AvlClk_k = ~AvlClk_k; //133.3MHz
logic StdClk_k = 0;
always #4 StdClk_k = ~StdClk_k; //125MHz

logic Clk40_k = 0;
always #12.5 Clk40_k = ~Clk40_k;

logic WbRst, WbCyc, WbStb, WbWe, WbAck;
logic [31:0] WbDatMiso_b32, WbDatMoSi_b32, WbAdr_b32;

logic AvlWaitRequest;
logic AvlReadDataValid = 0;
logic AvlRead, AvlWrite;
logic [10:0] AvlBurstCount_b11;
logic [15:0] AvlByteEnable_b16;
logic [25:0] AvlAddress_b26;
logic [127:0] AvlDataRead_b128 = 0, AvlDataWrite_b128;

logic [16:0] PageSelector_b17 = 0;
logic PageFetch_p = 0;
logic PageFetched;

logic [ 25:0] StdAddress_b26 = 26'h3FF_FFFF;
logic [127:0] StdData_b128;
logic [ 15:0] StdByteWe_b16 = 16'h0;
logic         StdWriteRedy;

logic        DdrInitMemory_p     = 0;
logic        DdrInitIgnoreWait   = 0;
logic        DdrInitLimit32      = 0;
logic [15:0] DdrByteWe_b16       = 0;
logic [ 7:0] DdrInitByte_b8      = 0;

logic [127:0] AvlMem_m2048b128 [16*1024-1:0];
integer ExpectedWords = 0;
integer ReceivedWords = 0;
integer WriteAddress = 0;

logic WrCycleStartAvlWait = 0;
logic RdCycleStartAvlWait = 0;
logic SimAvlWait = 0;
logic DdrInitAtLastAddress_a;

integer AddressForWbMaster;
logic [31:0] DataReadFromWb_b32;

//===========================================================================
//===========================================================================
//===========================================================================


initial begin
    StdByteWe_b16   <= 16'h0;
    StdData_b128    <= 128'h0;
    StdAddress_b26  <= 26'h0;
    repeat(20) @(posedge AvlClk_k);
    repeat(1024) begin
        @(posedge Clk40_k);
        @(posedge StdClk_k);
        StdByteWe_b16   <= 16'hFFFF;
        @(posedge StdClk_k);
        StdByteWe_b16   <= 16'h0;
        StdData_b128    <= StdData_b128 + 1'b1;
        StdAddress_b26  <= StdAddress_b26 + 1'b1;
    end
    repeat(20) @(posedge AvlClk_k);
    CheckMem(1024);
    PageSelector_b17 = 0;
    repeat(8) begin
        $display("Fetching page %d", PageSelector_b17);
        @(posedge WbClk_k);
        PageFetch_p = 1'b1;
        @(posedge WbClk_k);
        PageFetch_p = 1'b0;
        @(posedge PageFetched)  @(posedge WbClk_k);
        AddressForWbMaster = 0;
        repeat(1024) begin
            i_WbMaster.WbRead(AddressForWbMaster, DataReadFromWb_b32, 0);
            if (DataReadFromWb_b32 !==((AddressForWbMaster>>2)+(PageSelector_b17<<9))) begin
                $error("Expecting %d but read %d", ((AddressForWbMaster>>2)+(PageSelector_b17<<9)), DataReadFromWb_b32);
                $stop();
            end
            i_WbMaster.WbRead(AddressForWbMaster+1, DataReadFromWb_b32, 0);
            if (DataReadFromWb_b32 !==32'h0) $stop();
            i_WbMaster.WbRead(AddressForWbMaster+2, DataReadFromWb_b32, 0);
            if (DataReadFromWb_b32 !==32'h0) $stop();
            i_WbMaster.WbRead(AddressForWbMaster+3, DataReadFromWb_b32, 0);
            if (DataReadFromWb_b32 !==32'h0) $stop();
            AddressForWbMaster = AddressForWbMaster +4;
        end
        PageSelector_b17 = PageSelector_b17 + 1;
    end
    $display("Memory properely initialized");
    repeat(20) @(posedge AvlClk_k);
    $stop();
end

//===========================================================================
//===========================================================================
//===========================================================================


WbMasterSim #(.g_VerboseDefault(1))
    i_WbMaster (
        .Rst_orq  (WbRst),
        .Clk_ik   (WbClk_k),
        .Adr_obq32(WbAdr_b32),
        .Dat_obq32(WbDatMoSi_b32),
        .Dat_ib32 (WbDatMiso_b32),
        .We_oq    (WbWe),
        .Cyc_oq   (WbCyc),
        .Stb_oq   (WbStb),
        .Ack_i    (WbAck));

VfcDdr3Interface #(.g_Log2PageSize(11))
    i_DUT (
        .WbClk_ik          (WbClk_k),
        .WbCyc_i           (WbCyc),
        .WbStb_i           (WbStb),
        //.WbWe_i            (WbWe),
        .WbAdr_ib          (WbAdr_b32[10:0]),
        //.WbDat_ib32        (WbDatMoSi_b32),
        .WbDat_ob32        (WbDatMiso_b32),
        .WbAck_o           (WbAck),
        .PageSelector_ib   (PageSelector_b17),
        .FetchPage_ip      (PageFetch_p),
        .PageFetched_o     (PageFetched),
        .AvlClk_ik         (AvlClk_k),
        .AvlWaitRequest_i  (AvlWaitRequest),
        .AvlRead_o         (AvlRead),
        .AvlWrite_o        (AvlWrite),
        .AvlBurstCount_ob11(AvlBurstCount_b11),
        .AvlByteEnable_ob16(AvlByteEnable_b16),
        .AvlAddress_ob26   (AvlAddress_b26),
        .AvlReadDataValid_i (AvlReadDataValid),
        .AvlDataRead_ib128 (AvlDataRead_b128),
        .AvlDataWrite_ob128(AvlDataWrite_b128),
        .StdClk_ik         (StdClk_k),
        .StdAddress_ib26   (StdAddress_b26),
        .StdByteWe_ib16    (StdByteWe_b16),
        .StdData_ib128     (StdData_b128),
        .StdWriteReady_o   (StdWriteRedy));


//DDR with AVL interface simulation

always begin
    repeat(100) @(posedge AvlClk_k);
    SimAvlWait = 1;
    repeat(3) @(negedge AvlClk_k);
    SimAvlWait = 0;
end



assign AvlWaitRequest = WrCycleStartAvlWait || RdCycleStartAvlWait || SimAvlWait;

always @(posedge AvlClk_k) if (AvlWrite && AvlRead)
    $error("Concurrent read and Write requests");

always @(posedge AvlClk_k)
    if (AvlWrite && ~AvlRead && ~AvlWaitRequest) begin
        if (ExpectedWords==0) begin
            if (AvlBurstCount_b11 == 0) $error("Burst of 0 is not valid");
            else begin
                ExpectedWords = AvlBurstCount_b11;
                ReceivedWords = 1;
                WriteAddress =  AvlAddress_b26[8:0];
                AvlMem_m2048b128[AvlAddress_b26] = AvlDataWrite_b128;
                WriteAddress = WriteAddress + 1;
            end
        end else begin
            ReceivedWords = ReceivedWords + 1;
            AvlMem_m2048b128[AvlAddress_b26] = AvlDataWrite_b128;
            WriteAddress = WriteAddress + 1;
        end
        if (ExpectedWords==ReceivedWords) ExpectedWords = 0;
    end

always @(posedge AvlWrite) begin
    if (ExpectedWords==0) begin
        WrCycleStartAvlWait = 1;
        @(posedge AvlClk_k) #1 WrCycleStartAvlWait = 0;
    end
end

// for the read put a request FIFOs if there is the need to simulate multimaster

logic PauseRead = 0;
integer WordsToRead, ReadAddress;

always begin
    @(posedge AvlRead) RdCycleStartAvlWait = 1'b1;
    @(posedge AvlClk_k) #1 RdCycleStartAvlWait = 1'b0;
    @(posedge AvlClk_k) begin
        WordsToRead = AvlBurstCount_b11;
        ReadAddress = AvlAddress_b26;
    end
    while (WordsToRead > 0) begin
        @(posedge AvlClk_k);
        while (PauseRead) begin
            AvlReadDataValid = 0;
            @(posedge AvlClk_k);
        end
        #1;
        AvlReadDataValid = 1;
        AvlDataRead_b128 = AvlMem_m2048b128[ReadAddress];
        ReadAddress = ReadAddress +1;
        WordsToRead      = WordsToRead - 1;
    end
    @(posedge AvlClk_k) #1;
    AvlReadDataValid = 0;
end

task CheckMem(input integer CellsToCheck = 2048);
    logic [127:0] i;
    $display("Checking the memory content: %d locations to check", CellsToCheck);
    i = 0;
    repeat (CellsToCheck) begin
        if (AvlMem_m2048b128[i] !== i)
            $display("Error in cell %d: Content is %d", i, AvlMem_m2048b128[i]);
        i = i+1;
    end
endtask


endmodule
