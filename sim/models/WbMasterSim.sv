`timescale 1ns/100ps

module WbMasterSim #(
    parameter g_VerboseDefault = 0
) (
    output  reg        Rst_orq,
    input              Clk_ik,
    output  reg [31:0] Adr_obq32,
    output  reg [31:0] Dat_obq32,
    input       [31:0] Dat_ib32,
    output  reg        We_oq,
    output  reg        Cyc_oq,
    output  reg        Stb_oq,
    input              Ack_i);

// simulation checks
//assert property (@(posedge Clk_ik) ((Cyc_oq && Stb_oq) || ~Ack_i)) else $warning("WB signal ACK issued off the cycle by the slave!");

initial begin
  We_oq = 0;
  Cyc_oq = 0;
  Stb_oq = 0;
  Rst_orq = 0;
end


task WbWrite(input [31:0] Address_ib32, input [31:0] Data_ib32, input VerboseAccesses = g_VerboseDefault);
    if (VerboseAccesses) $display("WB write cycle start: d = 0x%h at address 0x%h at %t", Data_ib32, Address_ib32, $time);
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Cyc_oq      = 1'b1;
    Stb_oq      = 1'b1;
    We_oq       = 1'b1;
    Adr_obq32   = Address_ib32;
    Dat_obq32   = Data_ib32;
    if (~Ack_i) @(posedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Cyc_oq      = 1'b0;
    Stb_oq      = 1'b0;
    Dat_obq32   = 'hx;
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);
endtask

task WbRead (input [31:0] Address_ib32, output [31:0] Data_ob32, input VerboseAccesses = g_VerboseDefault);
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Cyc_oq      = 1'b1;
    Stb_oq      = 1'b1;
    We_oq       = 1'b0;
    Adr_obq32   = Address_ib32;
    Dat_obq32   = 'hx;
    if (~Ack_i) @(posedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Data_ob32   = Dat_ib32;
    Cyc_oq      = 1'b0;
    Stb_oq      = 1'b0;
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);
    if (VerboseAccesses) $display("WB read cycle done: d = 0x %h at address 0x%h at %t", Data_ob32, Address_ib32, $time);
endtask

task Reset();
	@(posedge Clk_ik) Rst_orq = #1 1;
	@(posedge Clk_ik) Rst_orq = #1 0;
endtask

endmodule
