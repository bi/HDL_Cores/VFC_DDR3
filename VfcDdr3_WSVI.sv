module VfcDdr3_WSVI (
    // Avalon Burst Interface interfaces
    t_AvalonMmInterface AvlBurstIntDdr3a_iot , // #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11))
    t_AvalonMmInterface AvlBurstIntDdr3b_iot , // #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11))
    output              AvlClk_ok,
    // controller clock and resets
    input               RefClk125Mhz_ik,
    input               Ddr3aResetGlobal_irn,
    input               Ddr3aResetSoft_irn,
    input               Ddr3bResetGlobal_irn,
    input               Ddr3bResetSoft_irn,
    // controller status
    output              Ddr3aInitDone_o,
    output              Ddr3aCalSuccess_o,
    output              Ddr3aCalFail_o,
    output              Ddr3bInitDone_o,
    output              Ddr3bCalSuccess_o,
    output              Ddr3bCalFail_o,
    // memory A connections
    output              Ddr3aCk_ok,
    output              Ddr3aCk_okn,
    output              Ddr3aCke_o,
    output              Ddr3aReset_orn,
    output              Ddr3aRas_on,
    output              Ddr3aCas_on,
    output              Ddr3aCs_on,
    output              Ddr3aWe_on,
    output              Ddr3aOdt_o,
    output [ 2:0]       Ddr3aBa_ob3,
    output [15:0]       Ddr3aAdr_ob16,
    output [ 1:0]       Ddr3aDm_ob2,
    inout  [ 1:0]       Ddr3aDqs_iob2,
    inout  [ 1:0]       Ddr3aDqs_iob2n,
    inout  [15:0]       Ddr3aDq_iob16,
    // memory B connections
    output              Ddr3bCk_ok,
    output              Ddr3bCk_okn,
    output              Ddr3bCke_o,
    output              Ddr3bReset_orn,
    output              Ddr3bRas_on,
    output              Ddr3bCas_on,
    output              Ddr3bCs_on,
    output              Ddr3bWe_on,
    output              Ddr3bOdt_o,
    output [ 2:0]       Ddr3bBa_ob3,
    output [15:0]       Ddr3bAdr_ob16,
    output [ 1:0]       Ddr3bDm_ob2,
    inout  [ 1:0]       Ddr3bDqs_iob2,
    inout  [ 1:0]       Ddr3bDqs_iob2n,
    inout  [15:0]       Ddr3bDq_iob16,
    // OCT control
    input  [15:0]       OctSeriesControl_ib16,
    input  [15:0]       OctParallelControl_ib16
);

VfcDdr3 i_VfcDdr3(
    .AvlClk_ok               ( AvlClk_ok                          ),
    .Ddr3aAvlAddress_ib26    ( AvlBurstIntDdr3a_iot.Address_b     ),
    .Ddr3aAvlReadDataValid_o ( AvlBurstIntDdr3a_iot.ReadDataValid ),
    .Ddr3aAvlDataRead_ob128  ( AvlBurstIntDdr3a_iot.DataRead_b    ),
    .Ddr3aAvlDataWrite_ib128 ( AvlBurstIntDdr3a_iot.DataWrite_b   ),
    .Ddr3aAvlByteEnable_ib16 ( AvlBurstIntDdr3a_iot.ByteEnable    ),
    .Ddr3aAvlRead_i          ( AvlBurstIntDdr3a_iot.Read          ),
    .Ddr3aAvlWrite_i         ( AvlBurstIntDdr3a_iot.Write         ),
    .Ddr3aAvlBurstCount_ib11 ( AvlBurstIntDdr3a_iot.BurstCount_b  ),
    .Ddr3aAvlWaitRequest_o   ( AvlBurstIntDdr3a_iot.WaitRequest   ),
    .Ddr3bAvlAddress_ib26    ( AvlBurstIntDdr3b_iot.Address_b     ),
    .Ddr3bAvlReadDataValid_o ( AvlBurstIntDdr3b_iot.ReadDataValid ),
    .Ddr3bAvlDataRead_ob128  ( AvlBurstIntDdr3b_iot.DataRead_b    ),
    .Ddr3bAvlDataWrite_ib128 ( AvlBurstIntDdr3b_iot.DataWrite_b   ),
    .Ddr3bAvlByteEnable_ib16 ( AvlBurstIntDdr3b_iot.ByteEnable    ),
    .Ddr3bAvlRead_i          ( AvlBurstIntDdr3b_iot.Read          ),
    .Ddr3bAvlWrite_i         ( AvlBurstIntDdr3b_iot.Write         ),
    .Ddr3bAvlBurstCount_ib11 ( AvlBurstIntDdr3b_iot.BurstCount_b  ),
    .Ddr3bAvlWaitRequest_o   ( AvlBurstIntDdr3b_iot.WaitRequest   ),
    .*);

endmodule
