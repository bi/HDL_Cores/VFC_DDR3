module TopVfcDdr3
#(
   parameter g_Log2PageSize = 'd6, // Comment: in 32bit words. Min is 2 as it must be 128bits aligned. Max is 12 as the longest burst is 1024
   parameter g_Log2StdInterfaceFifoDepth = 'd2 //minimum is 2 or the FIFO might have implementation issues
)
(
  //   WishBone READ ONLY interface
  input                          WbClk_ik,
  input                          WbCyc_i,
  input                          WbStb_i,
  input      [g_Log2PageSize :0] WbAdr_ib,        //1 bit more than page size as there are 2 modules
  output                  [31:0] WbDat_ob32,
  output                         WbAck_o,
  //   DDR3A specific controls
  input  [28-g_Log2PageSize-1:0] Ddr3aPageSelector_ib,
  input                          Ddr3aFetchPage_ip,
  output                         Ddr3aPageFetched_o,
  input                          Ddr3aStdClk_ik,
  input                   [25:0] Ddr3aStdAddress_ib26,
  input                   [15:0] Ddr3aStdByteWe_ib16,
  input              [15:0][7:0] Ddr3aStdData_ib128, //16 bytes
  output                         Ddr3aStdWriteReady_o,
  input                          Ddr3aResetGlobal_irn,
  input                          Ddr3aResetSoft_irn,
  output                         Ddr3aInitDone_o,
  output                         Ddr3aCalSuccess_o,
  output                         Ddr3aCalFail_o,
  // DDR3B specific controls
  input  [28-g_Log2PageSize-1:0] Ddr3bPageSelector_ib,
  input                          Ddr3bFetchPage_ip,
  output                         Ddr3bPageFetched_o,
  input                          Ddr3bStdClk_ik,
  input                   [25:0] Ddr3bStdAddress_ib26,
  input                   [15:0] Ddr3bStdByteWe_ib16,
  input              [15:0][7:0] Ddr3bStdData_ib128, //16 bytes
  output                         Ddr3bStdWriteReady_o,
  input                          Ddr3bResetGlobal_irn,
  input                          Ddr3bResetSoft_irn,
  output                         Ddr3bInitDone_o,
  output                         Ddr3bCalSuccess_o,
  output                         Ddr3bCalFail_o,
  // DDR3 common calibration and references
  input                          RefClk125Mhz_ik,
  input                  [ 15:0] OctSeriesControl_ib16,
  input                  [ 15:0] OctParallelControl_ib16,
  // External connections
  output                         Ddr3aCk_ok,
  output                         Ddr3aCk_okn,
  output                         Ddr3aCke_o,
  output                         Ddr3aReset_orn,
  output                         Ddr3aRas_on,
  output                         Ddr3aCas_on,
  output                         Ddr3aCs_on,
  output                         Ddr3aWe_on,
  output                         Ddr3aOdt_o,
  output                 [  2:0] Ddr3aBa_ob3,
  output                 [ 15:0] Ddr3aAdr_ob16,
  output                 [  1:0] Ddr3aDm_ob2,
  inout                  [  1:0] Ddr3aDqs_iob2,
  inout                  [  1:0] Ddr3aDqs_iob2n,
  inout                  [ 15:0] Ddr3aDq_iob16,
  output                         Ddr3bCk_ok,
  output                         Ddr3bCk_okn,
  output                         Ddr3bCke_o,
  output                         Ddr3bReset_orn,
  output                         Ddr3bRas_on,
  output                         Ddr3bCas_on,
  output                         Ddr3bCs_on,
  output                         Ddr3bWe_on,
  output                         Ddr3bOdt_o,
  output                 [  2:0] Ddr3bBa_ob3,
  output                 [ 15:0] Ddr3bAdr_ob16,
  output                 [  1:0] Ddr3bDm_ob2,
  inout                  [  1:0] Ddr3bDqs_iob2,
  inout                  [  1:0] Ddr3bDqs_iob2n,
  inout                  [ 15:0] Ddr3bDq_iob16);

logic Ddr3aWbStb, Ddr3bWbStb, Ddr3aWbAck, Ddr3bWbAck;
logic [31:0] Ddr3aWbDat_b32, Ddr3bWbDat_b32;

logic         AvlClk_k;
logic         Ddr3aAvlWaitRequest;
logic         Ddr3aAvlRead;
logic         Ddr3aAvlWrite;
logic  [10:0] Ddr3aAvlBurstCount_b11;
logic  [15:0] Ddr3aAvlByteEnable_b16;
logic  [25:0] Ddr3aAvlAddress_b26;
logic         Ddr3aAvlReadDataValid;
logic [127:0] Ddr3aAvlDataRead_b128;
logic [127:0] Ddr3aAvlDataWrite_b128;
logic         Ddr3bAvlWaitRequest;
logic         Ddr3bAvlRead;
logic         Ddr3bAvlWrite;
logic  [10:0] Ddr3bAvlBurstCount_b11;
logic  [15:0] Ddr3bAvlByteEnable_b16;
logic  [25:0] Ddr3bAvlAddress_b26;
logic         Ddr3bAvlReadDataValid;
logic [127:0] Ddr3bAvlDataRead_b128;
logic [127:0] Ddr3bAvlDataWrite_b128;


assign Ddr3aWbStb = WbAdr_ib[g_Log2PageSize] ?           1'b0 : WbStb_i;
assign Ddr3bWbStb = WbAdr_ib[g_Log2PageSize] ?        WbStb_i : 1'b0;
assign WbAck_o    = WbAdr_ib[g_Log2PageSize] ?     Ddr3bWbAck : Ddr3aWbAck;
assign WbDat_ob32 = WbAdr_ib[g_Log2PageSize] ? Ddr3bWbDat_b32 : Ddr3aWbDat_b32;

VfcDdr3Interface
    #(.g_Log2PageSize(g_Log2PageSize),
      .g_Log2StdInterfaceFifoDepth(g_Log2StdInterfaceFifoDepth))
    i_VfcDdr3aInterface (
        .WbClk_ik           ( WbClk_ik                     ),
        .WbCyc_i            ( WbCyc_i                      ),
        .WbStb_i            ( Ddr3aWbStb                   ),
        .WbAdr_ib           ( WbAdr_ib[g_Log2PageSize-1:0] ),
        .WbDat_ob32         ( Ddr3aWbDat_b32               ),
        .WbAck_o            ( Ddr3aWbAck                   ),
        .PageSelector_ib    ( Ddr3aPageSelector_ib         ),
        .FetchPage_ip       ( Ddr3aFetchPage_ip            ),
        .PageFetched_o      ( Ddr3aPageFetched_o           ),
        .StdClk_ik          ( Ddr3aStdClk_ik               ),
        .StdAddress_ib26    ( Ddr3aStdAddress_ib26         ),
        .StdByteWe_ib16     ( Ddr3aStdByteWe_ib16          ),
        .StdData_ib128      ( Ddr3aStdData_ib128           ),
        .StdWriteReady_o    ( Ddr3aStdWriteReady_o         ),
        .AvlClk_ik          ( AvlClk_k                     ),
        .AvlWaitRequest_i   ( Ddr3aAvlWaitRequest          ),
        .AvlRead_o          ( Ddr3aAvlRead                 ),
        .AvlWrite_o         ( Ddr3aAvlWrite                ),
        .AvlBurstCount_ob11 ( Ddr3aAvlBurstCount_b11       ),
        .AvlByteEnable_ob16 ( Ddr3aAvlByteEnable_b16       ),
        .AvlAddress_ob26    ( Ddr3aAvlAddress_b26          ),
        .AvlReadDataValid_i ( Ddr3aAvlReadDataValid        ),
        .AvlDataRead_ib128  ( Ddr3aAvlDataRead_b128        ),
        .AvlDataWrite_ob128 ( Ddr3aAvlDataWrite_b128       ));

VfcDdr3Interface
    #(.g_Log2PageSize(g_Log2PageSize),
      .g_Log2StdInterfaceFifoDepth(g_Log2StdInterfaceFifoDepth))
    i_VfcDdr3bInterface (
        .WbClk_ik           ( WbClk_ik                     ),
        .WbCyc_i            ( WbCyc_i                      ),
        .WbStb_i            ( Ddr3bWbStb                   ),
        .WbAdr_ib           ( WbAdr_ib[g_Log2PageSize-1:0] ),
        .WbDat_ob32         ( Ddr3bWbDat_b32               ),
        .WbAck_o            ( Ddr3bWbAck                   ),
        .PageSelector_ib    ( Ddr3bPageSelector_ib         ),
        .FetchPage_ip       ( Ddr3bFetchPage_ip            ),
        .PageFetched_o      ( Ddr3bPageFetched_o           ),
        .StdClk_ik          ( Ddr3bStdClk_ik               ),
        .StdAddress_ib26    ( Ddr3bStdAddress_ib26         ),
        .StdByteWe_ib16     ( Ddr3bStdByteWe_ib16          ),
        .StdData_ib128      ( Ddr3bStdData_ib128           ),
        .StdWriteReady_o    ( Ddr3bStdWriteReady_o         ),
        .AvlClk_ik          ( AvlClk_k                     ),
        .AvlWaitRequest_i   ( Ddr3bAvlWaitRequest          ),
        .AvlRead_o          ( Ddr3bAvlRead                 ),
        .AvlWrite_o         ( Ddr3bAvlWrite                ),
        .AvlBurstCount_ob11 ( Ddr3bAvlBurstCount_b11       ),
        .AvlByteEnable_ob16 ( Ddr3bAvlByteEnable_b16       ),
        .AvlAddress_ob26    ( Ddr3bAvlAddress_b26          ),
        .AvlReadDataValid_i ( Ddr3bAvlReadDataValid        ),
        .AvlDataRead_ib128  ( Ddr3bAvlDataRead_b128        ),
        .AvlDataWrite_ob128 ( Ddr3bAvlDataWrite_b128       ));

VfcDdr3 i_VfcDdr3(
    .AvlClk_ok              (AvlClk_k              ),
    .Ddr3aAvlAddress_ib26   (Ddr3aAvlAddress_b26   ),
    .Ddr3aAvlReadDataValid_o(Ddr3aAvlReadDataValid ),
    .Ddr3aAvlDataRead_ob128 (Ddr3aAvlDataRead_b128 ),
    .Ddr3aAvlDataWrite_ib128(Ddr3aAvlDataWrite_b128),
    .Ddr3aAvlByteEnable_ib16(Ddr3aAvlByteEnable_b16),
    .Ddr3aAvlRead_i         (Ddr3aAvlRead          ),
    .Ddr3aAvlWrite_i        (Ddr3aAvlWrite         ),
    .Ddr3aAvlBurstCount_ib11(Ddr3aAvlBurstCount_b11),
    .Ddr3aAvlWaitRequest_o  (Ddr3aAvlWaitRequest   ),
    .Ddr3bAvlAddress_ib26   (Ddr3bAvlAddress_b26   ),
    .Ddr3bAvlReadDataValid_o(Ddr3bAvlReadDataValid ),
    .Ddr3bAvlDataRead_ob128 (Ddr3bAvlDataRead_b128 ),
    .Ddr3bAvlDataWrite_ib128(Ddr3bAvlDataWrite_b128),
    .Ddr3bAvlByteEnable_ib16(Ddr3bAvlByteEnable_b16),
    .Ddr3bAvlRead_i         (Ddr3bAvlRead          ),
    .Ddr3bAvlWrite_i        (Ddr3bAvlWrite         ),
    .Ddr3bAvlBurstCount_ib11(Ddr3bAvlBurstCount_b11),
    .Ddr3bAvlWaitRequest_o  (Ddr3bAvlWaitRequest   ),
    .*);

endmodule
