/*
The DDR3 memory on the VFC-HD is 4Gb (per chip).
On the wishbone this is arranged as 128M Long Words (32 bits)
To address all the space 28 bits (27 in the prototypes) of address are needed
The address bits are split between a page address (PageSelector input)
and a pointer to the LW in the page that we want to access via WishBone
*/
module VfcDdr3WbToAvl
#(
    parameter      g_Log2PageSize = 'd6 // Comment: bits of address of the page number. It determines the size of the page
)
(
    t_WbInterface                WbSlave_iot,       //Comment: only g_Log2PageSize bits of address are used
    t_AvalonMmInterface          AvlMaster_iot,     // #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11))
    input  [28-g_Log2PageSize-1:0] PageSelector_ib
);

logic         WbDdr3WrReq_x       = 'd0;
logic         WbDdr3RdReq_x       = 'd0;
logic [  2:0] WbDdr3WrReq_d3      = 'd0;
logic [  2:0] WbDdr3RdReq_d3      = 'd0;
logic         AvlDdr3RdDone_x     = 'd0;
logic [  2:0] AvlDdr3RdDone_d3    = 'd0;
logic         WbDdr3AccessReq     = 'd0;
logic [127:0] AvlDdr3DataOut_b128 = 'd0;


//Fixed assignements
assign AvlMaster_iot.BurstCount_b = 11'h1;

//Logic on the wishbone clock
always_ff @(posedge WbSlave_iot.Clk_k)
    if (~WbSlave_iot.Cyc || ~WbSlave_iot.Stb) begin
        WbSlave_iot.Ack <= 1'b0;
        WbDdr3AccessReq <= 1'b0;
        AvlMaster_iot.ByteEnable <= 16'hFFFF;
    end else if (WbSlave_iot.We) begin
        AvlMaster_iot.Address_b <= {PageSelector_ib, WbSlave_iot.Adr_b[g_Log2PageSize-1:2]};
        WbSlave_iot.Ack         <= 1'b1;
        WbDdr3AccessReq         <= 1'b1;
        if (~WbDdr3AccessReq)   //First clock cycle in
            WbDdr3WrReq_x <= ~WbDdr3WrReq_x;
        case (WbSlave_iot.Adr_b[1:0])
            2'd0: begin
                AvlMaster_iot.DataWrite_b <= {96'h0, WbSlave_iot.DatMoSi_b};
                AvlMaster_iot.ByteEnable <= 16'h000F;
            end
            2'd1: begin
                AvlMaster_iot.DataWrite_b <= {64'h0, WbSlave_iot.DatMoSi_b, 32'h0};
                AvlMaster_iot.ByteEnable <= 16'h00F0;
            end
            2'd2: begin
                AvlMaster_iot.DataWrite_b <= {32'h0, WbSlave_iot.DatMoSi_b, 64'h0};
                AvlMaster_iot.ByteEnable <= 16'h0F00;
            end
            2'd3: begin
                AvlMaster_iot.DataWrite_b <= {WbSlave_iot.DatMoSi_b, 96'h0};
                AvlMaster_iot.ByteEnable <= 16'hF000;
            end
        endcase
    end else begin
        AvlMaster_iot.Address_b <= {PageSelector_ib, WbSlave_iot.Adr_b[g_Log2PageSize-1:2]};
        WbDdr3AccessReq         <= 1'b1;
        if (~WbDdr3AccessReq)   //First clock cycle in
            WbDdr3RdReq_x         <= ~WbDdr3RdReq_x;
        if (^AvlDdr3RdDone_d3[2:1])
            WbSlave_iot.Ack        <=  1'b1;
        case (WbSlave_iot.Adr_b[1:0])
            2'd0:
                WbSlave_iot.DatMiSo_b <= AvlDdr3DataOut_b128[31:0];
            2'd1:
                WbSlave_iot.DatMiSo_b <= AvlDdr3DataOut_b128[63:32];
            2'd2:
                WbSlave_iot.DatMiSo_b <= AvlDdr3DataOut_b128[95:64];
            2'd3:
                WbSlave_iot.DatMiSo_b <= AvlDdr3DataOut_b128[127:96];
        endcase
    end

always_ff @(posedge WbSlave_iot.Clk_k)
    AvlDdr3RdDone_d3 <= {AvlDdr3RdDone_d3[1:0], AvlDdr3RdDone_x};

//Logic on the avalon clock
always_ff @(posedge AvlMaster_iot.Clk_k) begin
    WbDdr3WrReq_d3 <= {WbDdr3WrReq_d3[1:0], WbDdr3WrReq_x};
    WbDdr3RdReq_d3 <= {WbDdr3RdReq_d3[1:0], WbDdr3RdReq_x};
end

always_ff @(posedge AvlMaster_iot.Clk_k)
    if (^WbDdr3WrReq_d3[2:1])
        AvlMaster_iot.Write <= 1'b1;
    else if (~AvlMaster_iot.WaitRequest)
        AvlMaster_iot.Write <= 1'b0;

always_ff @(posedge AvlMaster_iot.Clk_k)
    if (^WbDdr3RdReq_d3[2:1])
        AvlMaster_iot.Read <= 1'b1;
    else if (~AvlMaster_iot.WaitRequest)
        AvlMaster_iot.Read <= 1'b0;

always_ff @(posedge AvlMaster_iot.Clk_k)
    if (AvlMaster_iot.ReadDataValid) begin
        AvlDdr3DataOut_b128 <= AvlMaster_iot.DataRead_b;
        AvlDdr3RdDone_x     <= ~AvlDdr3RdDone_x;
    end

endmodule
