@echo off

REM generate MegaWizard IP cores
qmegawiz -silent INTENDED_DEVICE_FAMILY="Arria V" Ddr3M\Ddr3M.v
qmegawiz -silent INTENDED_DEVICE_FAMILY="Arria V" Ddr3S\Ddr3S.v
