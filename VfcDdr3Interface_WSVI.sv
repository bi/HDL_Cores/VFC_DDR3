module VfcDdr3Interface_WSVI
#(
   parameter g_Log2PageSize = 'd6, // Comment: in 32bit words. Min is 2 as it must be 128bits alligned. Max is 12 as the longest burst is 1024
   parameter g_Log2StdInterfaceFifoDepth = 'd2 //minimum is 2 or the FIFO might have implementation issues
)
(
  //   WishBone interface
  t_WbInterface                  WbSlave_iot      ,  // Comment: only g_Log2PageSize bits of address are used
  //   Paging mechanism controls
  input  [28-g_Log2PageSize-1:0] PageSelector_ib  ,
  input                          FetchPage_ip     ,
  output reg                     PageFetched_o = 0,
  //   Standard memory write interface
  input                          StdClk_ik        ,
  input                   [25:0] StdAddress_ib26  ,
  input                   [15:0] StdByteWe_ib16   ,
  input              [15:0][7:0] StdData_ib128    , //16 bytes
  output                         StdWriteReady_o  ,
  // Avalon Memory Mapped interface
  t_AvalonMmInterface            AvlMaster_iot     // #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11))
 );

 VfcDdr3Interface #(
        .g_Log2PageSize              ( g_Log2PageSize              ),
        .g_Log2StdInterfaceFifoDepth ( g_Log2StdInterfaceFifoDepth ))
    i_VfcDdr3Interface(
        .WbClk_ik           ( WbSlave_iot.Clk_k           ),
        .WbCyc_i            ( WbSlave_iot.Cyc             ),
        .WbStb_i            ( WbSlave_iot.Stb             ),
        .WbAdr_ib           ( WbSlave_iot.Adr_b           ),
        .WbDat_ob32         ( WbSlave_iot.DatMiSo_b       ),
        .WbAck_o            ( WbSlave_iot.Ack             ),
        .PageSelector_ib    ( PageSelector_ib             ),
        .FetchPage_ip       ( FetchPage_ip                ),
        .PageFetched_o      ( PageFetched_o               ),
        .AvlClk_ik          ( AvlMaster_iot.Clk_k         ),
        .AvlWaitRequest_i   ( AvlMaster_iot.WaitRequest   ),
        .AvlRead_o          ( AvlMaster_iot.Read          ),
        .AvlWrite_o         ( AvlMaster_iot.Write         ),
        .AvlBurstCount_ob11 ( AvlMaster_iot.BurstCount_b  ),
        .AvlByteEnable_ob16 ( AvlMaster_iot.ByteEnable    ),
        .AvlAddress_ob26    ( AvlMaster_iot.Address_b     ),
        .AvlReadDataValid_i ( AvlMaster_iot.ReadDataValid ),
        .AvlDataRead_ib128  ( AvlMaster_iot.DataRead_b    ),
        .AvlDataWrite_ob128 ( AvlMaster_iot.DataWrite_b   ),
        .StdClk_ik          ( StdClk_ik                   ),
        .StdAddress_ib26    ( StdAddress_ib26             ),
        .StdByteWe_ib16     ( StdByteWe_ib16              ),
        .StdData_ib128      ( StdData_ib128               ),
        .StdWriteReady_o    ( StdWriteReady_o             ));

endmodule
