/*
The DDR3 memory on the VFC-HD is 4Gb (per chip).
On the wishbone this is arranged as 128M Long Words (32 bits)
To address all the space 28 (27 for the prototype) bits of address are needed
The address bits are split between a page address (PageSelector input)
and a pointer to the LW in the page that we want to access via WishBone
*/
module VfcDdr3Interface
#(
   parameter g_Log2PageSize = 'd6, // Comment: in 32bit words. Min is 2 as it must be 128bits aligned. Max is 12 as the longest burst is 1024
   parameter g_Log2StdInterfaceFifoDepth = 'd2 //minimum is 2 or the FIFO might have implementation issues
)
(
  //   WishBone READ ONLY interface
  input                          WbClk_ik,
  input                          WbCyc_i,
  input                          WbStb_i,
  input     [g_Log2PageSize-1:0] WbAdr_ib,
  output reg              [31:0] WbDat_ob32 = 0,
  output reg                     WbAck_o = 0,
  //   Paging mechanism controls
  input  [28-g_Log2PageSize-1:0] PageSelector_ib,
  input                          FetchPage_ip,
  output reg                     PageFetched_o = 0,
  //   Standard memory write interface
  input                          StdClk_ik,
  input                   [25:0] StdAddress_ib26,
  input                   [15:0] StdByteWe_ib16,
  input              [15:0][7:0] StdData_ib128, //16 bytes
  output                         StdWriteReady_o,
  //   Avalon MM burst enabled interface
  input                          AvlClk_ik,
  input                          AvlWaitRequest_i,
  output reg                     AvlRead_o = 0,
  output reg                     AvlWrite_o = 0,
  output reg              [10:0] AvlBurstCount_ob11 = 0,
  output reg              [15:0] AvlByteEnable_ob16 = 0,
  output reg              [25:0] AvlAddress_ob26 = 0,
  input                          AvlReadDataValid_i,
  input                  [127:0] AvlDataRead_ib128,
  output reg             [127:0] AvlDataWrite_ob128 = 0);


localparam c_PageMemDepth   = 2**(g_Log2PageSize-2);

typedef enum {
    s_AvlIdle,
    s_AvlPageRead,
    s_AvlStdWrite
} t_AvlState;

logic [127:0] PageMem_mb128 [c_PageMemDepth-1:0];
logic StdWriteFifoFull, WeStdWriteFifo, ReStdWriteFifo, StdWriteFifoEmpty;
logic [25:0] MemoryStdWriteAddress_b26;
logic [15:0] MemoryStdWriteEnable_b16;
logic [127:0] MemoryStdWriteData_b128;
t_AvlState AvlState_l = s_AvlIdle, AvlNextState_l, AvlState_ld;
logic FetchPage_x = 0;
logic [2:0] FetchPage_xd3 = 0;
logic FetchDone_x = 0;
logic [2:0] FetchDone_xd3 = 0;
logic FetchRequest;
logic [28-g_Log2PageSize-1:0] PageToFetch_b;
logic [(g_Log2PageSize-2)-1:0] MemWrPointer_c;
logic [25:0] MemoryWbWriteAddress_b26;
logic [1:0] MemoryWbWriteLwSelect_b2;
logic [31:0] MemoryWbWriteLw_b32;
logic FetchDone_p = 0;

//=== WishBone interface
always_ff @(posedge WbClk_ik)
    if (WbCyc_i && WbStb_i && ~WbAck_o) begin
        case (WbAdr_ib[1:0])
            2'd0: WbDat_ob32 <= PageMem_mb128[WbAdr_ib[g_Log2PageSize-1:2]][ 31: 0];
            2'd1: WbDat_ob32 <= PageMem_mb128[WbAdr_ib[g_Log2PageSize-1:2]][ 63:32];
            2'd2: WbDat_ob32 <= PageMem_mb128[WbAdr_ib[g_Log2PageSize-1:2]][ 95:64];
            2'd3: WbDat_ob32 <= PageMem_mb128[WbAdr_ib[g_Log2PageSize-1:2]][127:96];
        endcase
        WbAck_o <= 1'b1;
    end else begin
        WbAck_o <= WbCyc_i && WbStb_i;
    end

//=== Synchronization logic for the paging fetch signals

always_ff @(posedge WbClk_ik)
    if (FetchPage_ip) begin
        FetchPage_x   <= ~FetchPage_x;
        PageToFetch_b <= PageSelector_ib;
    end

always_ff @(posedge AvlClk_ik)
  FetchPage_xd3 <= {FetchPage_xd3[1:0], FetchPage_x};

wire FetchPageAvl_p = ^FetchPage_xd3[2:1];

always_ff @(posedge AvlClk_ik)
    if (FetchDone_p)
        FetchDone_x <= ~FetchDone_x;

always_ff @(posedge WbClk_ik)
    FetchDone_xd3 <= {FetchDone_xd3[1:0], FetchDone_x};

wire FetchDoneWb_p = ^FetchDone_xd3[2:1];

always_ff @(posedge WbClk_ik)
    if (FetchPage_ip)
        PageFetched_o <= 1'b0;
    else if (FetchDoneWb_p)
        PageFetched_o <= 1'b1;

//=== Standard interface
assign StdWriteReady_o = ~StdWriteFifoFull;
assign WeStdWriteFifo = |StdByteWe_ib16 && StdWriteReady_o;

logic [169:0] StdWriteFifoOut_b170;
logic FifoOutMemFull;

generic_fifo_dc_gray_mod #(.dw(170), .aw(g_Log2StdInterfaceFifoDepth))
    i_StandardFifo(
        .rd_clk(AvlClk_ik),
        .wr_clk(StdClk_ik),
        .rst(1'b1),
        .clr(1'b0),
        .din({StdByteWe_ib16, StdAddress_ib26, StdData_ib128}),
        .we(WeStdWriteFifo),
        .dout(StdWriteFifoOut_b170),
        .re(ReStdWriteFifo),
        .full(StdWriteFifoFull),
        .empty(StdWriteFifoEmpty),
        .wr_level(/* Not Connected */),
        .rd_level(/* Not Connected */));

assign ReStdWriteFifo = ~FifoOutMemFull && (AvlState_l==s_AvlStdWrite) && ~StdWriteFifoEmpty;

//=== Avalon Burst MM interface
logic [170:0] FifoOutMem_mb170 [7:0];
logic [  7:0] FifoOutMemDav_b = 8'b0;
logic [  2:0] FifoOutWrPointer_b = 0;
logic [  2:0] FifoOutRdPointer_b = 0;
logic StdWrFifoDav = 0;

always_ff @(posedge AvlClk_ik)
  AvlState_l <= AvlNextState_l;

always_comb begin
    AvlNextState_l = AvlState_l;
    case (AvlState_l)
    s_AvlIdle:
        if      (FetchRequest)       AvlNextState_l = s_AvlPageRead;
        else if (~StdWriteFifoEmpty || ~|FifoOutMemDav_b) AvlNextState_l = s_AvlStdWrite;
    s_AvlStdWrite:
        if (StdWriteFifoEmpty && ~|FifoOutMemDav_b && ~AvlWaitRequest_i && ~StdWrFifoDav)
            AvlNextState_l = s_AvlIdle;
    s_AvlPageRead:
        if (&MemWrPointer_c && AvlReadDataValid_i)
            AvlNextState_l = s_AvlIdle;
    default:
        AvlNextState_l = s_AvlIdle;
    endcase
end

always_ff @(posedge AvlClk_ik) begin
    if (FetchPageAvl_p)
        FetchRequest   <= 1'b1;
    AvlState_ld    <= AvlState_l;
    FetchDone_p    <= 1'b0;
    FifoOutMemFull <= 1'b0;
    StdWrFifoDav   <= ReStdWriteFifo;
    case (AvlState_l)
    s_AvlIdle: begin
        MemWrPointer_c <= 'h0;
        AvlRead_o      <= 1'b0;
        AvlWrite_o     <= 1'b0;
        if (AvlNextState_l == s_AvlPageRead)
            FetchRequest <= 1'b0;
    end
    s_AvlPageRead: begin
        if (AvlState_ld==s_AvlIdle) //first cycle in state
            AvlRead_o <= 1'b1;
        else if (~AvlWaitRequest_i) //Request accepted
            AvlRead_o <= 1'b0;
        if (AvlNextState_l == s_AvlIdle)
            FetchDone_p                             <= 1'b1;
            AvlBurstCount_ob11                      <= c_PageMemDepth[10:0];
            AvlByteEnable_ob16                      <= 16'hFFFF;
            AvlAddress_ob26[25:(g_Log2PageSize-2)]  <= PageToFetch_b;
            AvlAddress_ob26[(g_Log2PageSize-2)-1:0] <= 'b0;
        if (AvlReadDataValid_i) begin
            PageMem_mb128[MemWrPointer_c] <= AvlDataRead_ib128;
            MemWrPointer_c                <= MemWrPointer_c + 1'b1;
        end
    end
    s_AvlStdWrite: begin
        //Writing in the circular buffer
        FifoOutMemFull <= FifoOutMemDav_b[FifoOutWrPointer_b] || FifoOutMemDav_b[FifoOutWrPointer_b+2'd1] || FifoOutMemDav_b[FifoOutWrPointer_b+2'd2];
        if (StdWrFifoDav) begin
            FifoOutMemDav_b  [FifoOutWrPointer_b] <= 1'b1;
            FifoOutMem_mb170 [FifoOutWrPointer_b] <= StdWriteFifoOut_b170;
            FifoOutWrPointer_b                    <= FifoOutWrPointer_b + 1'b1;
        end
        //Reading from the circular buffer and AvlMMB cyle generation
        if (~AvlWrite_o && FifoOutMemDav_b[FifoOutRdPointer_b]) begin //No cycle was already running
            AvlWrite_o                          <= 1'b1;
            AvlBurstCount_ob11                  <= 11'd1;
            AvlDataWrite_ob128                  <= FifoOutMem_mb170[FifoOutRdPointer_b][127:0];
            AvlAddress_ob26                     <= FifoOutMem_mb170[FifoOutRdPointer_b][153:128];
            AvlByteEnable_ob16                  <= FifoOutMem_mb170[FifoOutRdPointer_b][169:154];
            FifoOutMemDav_b[FifoOutRdPointer_b] <= 1'b0;
            FifoOutRdPointer_b                  <= FifoOutRdPointer_b + 1'b1;
        end else if (AvlWrite_o && ~AvlWaitRequest_i) begin  //A write request was on and accepted
            if (FifoOutMemDav_b[FifoOutRdPointer_b]) begin //More to write
                AvlWrite_o                          <= 1'b1;
                AvlBurstCount_ob11                  <= 11'd1;
                AvlDataWrite_ob128                  <= FifoOutMem_mb170[FifoOutRdPointer_b][127:0];
                AvlAddress_ob26                     <= FifoOutMem_mb170[FifoOutRdPointer_b][153:128];
                AvlByteEnable_ob16                  <= FifoOutMem_mb170[FifoOutRdPointer_b][169:154];
                FifoOutMemDav_b[FifoOutRdPointer_b] <= 1'b0;
                FifoOutRdPointer_b                  <= FifoOutRdPointer_b + 1'b1;
            end else begin //Waiting for data to be fetched from the FIFO into the circular buffer
                AvlWrite_o         <= 1'b0;
                AvlBurstCount_ob11 <= 11'd1;
                AvlDataWrite_ob128 <= FifoOutMem_mb170[FifoOutRdPointer_b][127:0];
                AvlAddress_ob26    <= FifoOutMem_mb170[FifoOutRdPointer_b][153:128];
                AvlByteEnable_ob16 <= FifoOutMem_mb170[FifoOutRdPointer_b][169:154];
            end
        end
    end
    default: begin
        MemWrPointer_c <= 'h0;
        AvlRead_o      <= 1'b0;
        AvlWrite_o     <= 1'b0;
    end
    endcase
end

// not used?
// wire DebugRdPointedDav  = FifoOutMemDav_b[FifoOutRdPointer_b];
// wire DebugWrPointedDav0 = FifoOutMemDav_b[FifoOutWrPointer_b];
// wire DebugWrPointedDav1 = FifoOutMemDav_b[FifoOutWrPointer_b+ 2'd1];
// wire DebugWrPointedDav2 = FifoOutMemDav_b[FifoOutWrPointer_b+ 2'd2];


endmodule
