module VfcDdr3 (
    // Avalon Burst Interface interfaces
    output         AvlClk_ok, //Clock common to the 2 interfaces
    //DDR3 A Avl interface
    input  [ 25:0] Ddr3aAvlAddress_ib26,
    output         Ddr3aAvlReadDataValid_o,
    output [127:0] Ddr3aAvlDataRead_ob128,
    input  [127:0] Ddr3aAvlDataWrite_ib128,
    input  [ 15:0] Ddr3aAvlByteEnable_ib16,
    input          Ddr3aAvlRead_i,
    input          Ddr3aAvlWrite_i,
    input  [ 10:0] Ddr3aAvlBurstCount_ib11,
    output         Ddr3aAvlWaitRequest_o,
    //DDR3 B Avl interface
    input  [ 25:0] Ddr3bAvlAddress_ib26,
    output         Ddr3bAvlReadDataValid_o,
    output [127:0] Ddr3bAvlDataRead_ob128,
    input  [127:0] Ddr3bAvlDataWrite_ib128,
    input  [ 15:0] Ddr3bAvlByteEnable_ib16,
    input          Ddr3bAvlRead_i,
    input          Ddr3bAvlWrite_i,
    input  [ 10:0] Ddr3bAvlBurstCount_ib11,
    output         Ddr3bAvlWaitRequest_o,
    // controller clock and resets
    input          RefClk125Mhz_ik,
    input          Ddr3aResetGlobal_irn,
    input          Ddr3aResetSoft_irn,
    input          Ddr3bResetGlobal_irn,
    input          Ddr3bResetSoft_irn,
    // controller status
    output         Ddr3aInitDone_o,
    output         Ddr3aCalSuccess_o,
    output         Ddr3aCalFail_o,
    output         Ddr3bInitDone_o,
    output         Ddr3bCalSuccess_o,
    output         Ddr3bCalFail_o,
    // memory A connections
    output         Ddr3aCk_ok,
    output         Ddr3aCk_okn,
    output         Ddr3aCke_o,
    output         Ddr3aReset_orn,
    output         Ddr3aRas_on,
    output         Ddr3aCas_on,
    output         Ddr3aCs_on,
    output         Ddr3aWe_on,
    output         Ddr3aOdt_o,
    output [  2:0] Ddr3aBa_ob3,
    output [ 15:0] Ddr3aAdr_ob16,
    output [  1:0] Ddr3aDm_ob2,
    inout  [  1:0] Ddr3aDqs_iob2,
    inout  [  1:0] Ddr3aDqs_iob2n,
    inout  [ 15:0] Ddr3aDq_iob16,
    // memory B connections
    output         Ddr3bCk_ok,
    output         Ddr3bCk_okn,
    output         Ddr3bCke_o,
    output         Ddr3bReset_orn,
    output         Ddr3bRas_on,
    output         Ddr3bCas_on,
    output         Ddr3bCs_on,
    output         Ddr3bWe_on,
    output         Ddr3bOdt_o,
    output [  2:0] Ddr3bBa_ob3,
    output [ 15:0] Ddr3bAdr_ob16,
    output [  1:0] Ddr3bDm_ob2,
    inout  [  1:0] Ddr3bDqs_iob2,
    inout  [  1:0] Ddr3bDqs_iob2n,
    inout  [ 15:0] Ddr3bDq_iob16,
    // OCT control
    input  [ 15:0] OctSeriesControl_ib16,
    input  [ 15:0] OctParallelControl_ib16);

logic       Ddr3AfiClk_k                  ;
logic       Ddr3AfiHalfClk_k              ;
logic       Ddr3AfiReset_rn               ;
logic       SharingPllMemClk_k            ;
logic       SharingPllWriteClk_k          ;
logic       SharingPllLocked              ;
logic       SharingPllWriteClkPrePhyClk_k ;
logic       SharingPllAddrCmdClk_k        ;
logic       SharingPllAvlClk_k            ;
logic       SharingPllConfigClk_k         ;
logic       SharingPllHrClk_k             ;
logic       SharingPllMemPhyClk_k         ;
logic       SharingAfiPhyClk_k            ;
logic       SharingPllAvlPhyClk_k         ;
logic       SharingDllPllLocked           ;
logic [6:0] SharingDllDelayctrl           ;
logic       AvlDdr3aReady                 ;
logic       AvlDdr3bReady                 ;

assign AvlClk_ok             = Ddr3AfiClk_k;
assign Ddr3aAvlWaitRequest_o = ~AvlDdr3aReady;
assign Ddr3bAvlWaitRequest_o = ~AvlDdr3bReady;

Ddr3M i_Ddr3M (
    .pll_ref_clk                ( RefClk125Mhz_ik               ),
    .global_reset_n             ( Ddr3aResetGlobal_irn          ),
    .soft_reset_n               ( Ddr3aResetSoft_irn            ),
    .afi_clk                    ( Ddr3AfiClk_k                  ),
    .afi_half_clk               ( Ddr3AfiHalfClk_k              ),
    .afi_reset_n                ( Ddr3AfiReset_rn               ),
    .afi_reset_export_n         ( /* unconnected */             ),
    .mem_a                      ( Ddr3aAdr_ob16                 ),
    .mem_ba                     ( Ddr3aBa_ob3                   ),
    .mem_ck                     ( Ddr3aCk_ok                    ),
    .mem_ck_n                   ( Ddr3aCk_okn                   ),
    .mem_cke                    ( Ddr3aCke_o                    ),
    .mem_cs_n                   ( Ddr3aCs_on                    ),
    .mem_dm                     ( Ddr3aDm_ob2                   ),
    .mem_ras_n                  ( Ddr3aRas_on                   ),
    .mem_cas_n                  ( Ddr3aCas_on                   ),
    .mem_we_n                   ( Ddr3aWe_on                    ),
    .mem_reset_n                ( Ddr3aReset_orn                ),
    .mem_dq                     ( Ddr3aDq_iob16                 ),
    .mem_dqs                    ( Ddr3aDqs_iob2                 ),
    .mem_dqs_n                  ( Ddr3aDqs_iob2n                ),
    .mem_odt                    ( Ddr3aOdt_o                    ),
    .avl_ready                  ( AvlDdr3aReady                 ),
    .avl_burstbegin             ( 1'b0                          ), //Comment: Deprecated signal that altera suggest to keep to gnd
    .avl_addr                   ( Ddr3aAvlAddress_ib26          ),
    .avl_rdata_valid            ( Ddr3aAvlReadDataValid_o       ),
    .avl_rdata                  ( Ddr3aAvlDataRead_ob128        ),
    .avl_wdata                  ( Ddr3aAvlDataWrite_ib128       ),
    .avl_be                     ( Ddr3aAvlByteEnable_ib16       ),
    .avl_read_req               ( Ddr3aAvlRead_i                ),
    .avl_write_req              ( Ddr3aAvlWrite_i               ),
    .avl_size                   ( Ddr3aAvlBurstCount_ib11       ),
    .local_init_done            ( Ddr3aInitDone_o               ),
    .local_cal_success          ( Ddr3aCalSuccess_o             ),
    .local_cal_fail             ( Ddr3aCalFail_o                ),
    .seriesterminationcontrol   ( OctSeriesControl_ib16         ),
    .parallelterminationcontrol ( OctParallelControl_ib16       ),
    .pll_mem_clk                ( SharingPllMemClk_k            ),
    .pll_write_clk              ( SharingPllWriteClk_k          ),
    .pll_locked                 ( SharingPllLocked              ),
    .pll_write_clk_pre_phy_clk  ( SharingPllWriteClkPrePhyClk_k ),
    .pll_addr_cmd_clk           ( SharingPllAddrCmdClk_k        ),
    .pll_avl_clk                ( SharingPllAvlClk_k            ),
    .pll_config_clk             ( SharingPllConfigClk_k         ),
    .pll_hr_clk                 ( SharingPllHrClk_k             ),
    .pll_mem_phy_clk            ( SharingPllMemPhyClk_k         ),
    .afi_phy_clk                ( SharingAfiPhyClk_k            ),
    .pll_avl_phy_clk            ( SharingPllAvlPhyClk_k         ),
    .dll_pll_locked             ( SharingDllPllLocked           ),
    .dll_delayctrl              ( SharingDllDelayctrl           ));

Ddr3S i_Ddr3S (
    .global_reset_n             ( Ddr3bResetGlobal_irn          ),
    .soft_reset_n               ( Ddr3bResetSoft_irn            ),
    .afi_clk                    ( Ddr3AfiClk_k                  ),
    .afi_half_clk               ( Ddr3AfiHalfClk_k              ),
    .afi_reset_n                ( Ddr3AfiReset_rn               ),
    .mem_a                      ( Ddr3bAdr_ob16                 ),
    .mem_ba                     ( Ddr3bBa_ob3                   ),
    .mem_ck                     ( Ddr3bCk_ok                    ),
    .mem_ck_n                   ( Ddr3bCk_okn                   ),
    .mem_cke                    ( Ddr3bCke_o                    ),
    .mem_cs_n                   ( Ddr3bCs_on                    ),
    .mem_dm                     ( Ddr3bDm_ob2                   ),
    .mem_ras_n                  ( Ddr3bRas_on                   ),
    .mem_cas_n                  ( Ddr3bCas_on                   ),
    .mem_we_n                   ( Ddr3bWe_on                    ),
    .mem_reset_n                ( Ddr3bReset_orn                ),
    .mem_dq                     ( Ddr3bDq_iob16                 ),
    .mem_dqs                    ( Ddr3bDqs_iob2                 ),
    .mem_dqs_n                  ( Ddr3bDqs_iob2n                ),
    .mem_odt                    ( Ddr3bOdt_o                    ),
    .avl_ready                  ( AvlDdr3bReady                 ),
    .avl_burstbegin             ( 1'b0                          ), //Comment: Deprecated signal that altera suggest to keep to gnd
    .avl_addr                   ( Ddr3bAvlAddress_ib26          ),
    .avl_rdata_valid            ( Ddr3bAvlReadDataValid_o       ),
    .avl_rdata                  ( Ddr3bAvlDataRead_ob128        ),
    .avl_wdata                  ( Ddr3bAvlDataWrite_ib128       ),
    .avl_be                     ( Ddr3bAvlByteEnable_ib16       ),
    .avl_read_req               ( Ddr3bAvlRead_i                ),
    .avl_write_req              ( Ddr3bAvlWrite_i               ),
    .avl_size                   ( Ddr3bAvlBurstCount_ib11       ),
    .local_init_done            ( Ddr3bInitDone_o               ),
    .local_cal_success          ( Ddr3bCalSuccess_o             ),
    .local_cal_fail             ( Ddr3bCalFail_o                ),
    .seriesterminationcontrol   ( OctSeriesControl_ib16         ),
    .parallelterminationcontrol ( OctParallelControl_ib16       ),
    .pll_mem_clk                ( SharingPllMemClk_k            ),
    .pll_write_clk              ( SharingPllWriteClk_k          ),
    .pll_locked                 ( SharingPllLocked              ),
    .pll_write_clk_pre_phy_clk  ( SharingPllWriteClkPrePhyClk_k ),
    .pll_addr_cmd_clk           ( SharingPllAddrCmdClk_k        ),
    .pll_avl_clk                ( SharingPllAvlClk_k            ),
    .pll_config_clk             ( SharingPllConfigClk_k         ),
    .pll_hr_clk                 ( SharingPllHrClk_k             ),
    .pll_mem_phy_clk            ( SharingPllMemPhyClk_k         ),
    .afi_phy_clk                ( SharingAfiPhyClk_k            ),
    .pll_avl_phy_clk            ( SharingPllAvlPhyClk_k         ),
    .dll_pll_locked             ( SharingDllPllLocked           ),
    .dll_delayctrl              ( SharingDllDelayctrl           ));

endmodule
