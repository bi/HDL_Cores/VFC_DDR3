/*
The DDR3 memory on the VFC-HD is 4Gb (per chip).
On the wishbone this is arranged as 128M Long Words (32 bits)
To address all the space 28 (27 for the prototype) bits of address are needed
The address bits are split between a page address (PageSelector input)
and a pointer to the LW in the page that we want to access via WishBone
*/
module VfcDdr3WbToAvlWithFetch
#(
   parameter g_Log2PageSize = 'd6 // Comment: in 32bit words. Min is 2 as it must be 128bits alligned. Max is 12 as the longest burst is 1024
)
(
  t_WbInterface                  WbSlave_iot,     // Comment: only g_Log2PageSize bits of address are used
  t_AvalonMmInterface            AvlMaster_iot,   // #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11))
  input  [28-g_Log2PageSize-1:0] PageSelector_ib,
  output                         WriteFifoEmpty_o,
  input                          FetchPage_ip,
  output reg                     PageFetched_o
);

parameter [10:0] c_PageMemDepth  = 2**(g_Log2PageSize-2);
typedef enum {
    s_AvlIdle,
    s_AvlPageRead,
    s_AvlSingleWrite
} t_AvlState;

logic [127:0] PageMem_mb128 [c_PageMemDepth-1:0];
logic WriteFifoFull, WeWriteFifo;
t_AvlState AvlState_l = s_AvlIdle, AvlNextState_l, AvlState_ld;
logic FetchPage_x = 0;
logic [2:0] FetchPage_xd3 = 0;
logic FetchPage_p = 0;
logic FetchDone_x = 0;
logic [2:0] FetchDone_xd3 = 0;
logic FetchRequest;
logic [28-g_Log2PageSize-1:0] PageToFetch_b;
logic [(g_Log2PageSize-2)-1:0] MemWrPointer_c;
logic [25:0] MemoryWriteAddress_b26;
logic [1:0] MemoryWriteLwSelect_b2;
logic [31:0] MemoryWriteLw_b32;
logic ReWriteFifo;
logic FetchDone_p = 0;

wire a_WbClk_k  = WbSlave_iot.Clk_k;
wire a_AvlClk_k = AvlMaster_iot.Clk_k;

//=== WishBone interface
always @(posedge a_WbClk_k)
    if (WbSlave_iot.Cyc && WbSlave_iot.Stb) begin //Wishbone access
        if (WbSlave_iot.We) begin //write access
            if (~WriteFifoFull && ~WbSlave_iot.Ack) begin
                WeWriteFifo     <= 1'b1;
                WbSlave_iot.Ack <= 1'b1;
            end else begin
                WeWriteFifo <= 1'b0;
            end
        end else begin //read access
            case (WbSlave_iot.Adr_b[1:0])
                2'd0: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][ 31: 0];
                2'd1: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][ 63:32];
                2'd2: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][ 95:64];
                2'd3: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][127:96];
            endcase
            WbSlave_iot.Ack <= 1'b1;
        end
    end else begin
        WbSlave_iot.Ack <= 1'b0;
        WeWriteFifo     <= 1'b0;
    end//Wb access closed or simply no access

generic_fifo_dc_gray_mod #(.dw(60), .aw(4))
    i_WriteFifo(
        .rd_clk(a_AvlClk_k),
        .wr_clk(a_WbClk_k),
        .rst(1'b1),
        .clr(1'b0),
        .din({PageSelector_ib, WbSlave_iot.Adr_b[g_Log2PageSize-1:0], WbSlave_iot.DatMoSi_b}),
        .we(WeWriteFifo),
		.dout({MemoryWriteAddress_b26, MemoryWriteLwSelect_b2, MemoryWriteLw_b32}),
        .re(ReWriteFifo),
        .full(WriteFifoFull),
        .empty(WriteFifoEmpty_o),
        .wr_level(/* Not Connected */),
        .rd_level(/* Not Connected */)
        );

//=== Synchronization logic

always_ff @(posedge a_WbClk_k)
    if (FetchPage_ip) begin
        FetchPage_x   <= ~FetchPage_x;
        PageToFetch_b <= PageSelector_ib;
    end

always_ff @(posedge a_AvlClk_k)
  FetchPage_xd3 <= {FetchPage_xd3[1:0], FetchPage_x};

wire FetchPageAvl_p = ^FetchPage_xd3[2:1];

always_ff @(posedge a_AvlClk_k)
    if (FetchDone_p)
        FetchDone_x <= ~FetchDone_x;

always_ff @(posedge a_WbClk_k)
    FetchDone_xd3 <= {FetchDone_xd3[1:0], FetchDone_x};

wire FetchDoneWb_p = ^FetchDone_xd3[2:1];

always_ff @(posedge a_WbClk_k)
    if (FetchPage_ip)
        PageFetched_o <= 1'b0;
    else if (FetchDoneWb_p)
        PageFetched_o <= 1'b1;

//=== Avalon Burst MM interface
always_ff @(posedge a_AvlClk_k)
  AvlState_l <= AvlNextState_l;

always_comb begin
    AvlNextState_l = AvlState_l;
    case (AvlState_l)
    s_AvlIdle:
        if (~WriteFifoEmpty_o && ~ReWriteFifo)   AvlNextState_l = s_AvlSingleWrite;
        else if (FetchRequest) AvlNextState_l = s_AvlPageRead;
    s_AvlSingleWrite:
        if (~AvlMaster_iot.WaitRequest)
            AvlNextState_l = s_AvlIdle;
    s_AvlPageRead:
        if (&MemWrPointer_c && ~AvlMaster_iot.WaitRequest)
            AvlNextState_l = s_AvlIdle;
    default:
        AvlNextState_l = s_AvlIdle;
    endcase
end

always_ff @(posedge a_AvlClk_k) begin
    FetchRequest <= FetchPageAvl_p;
    ReWriteFifo  <= 1'b0;
    AvlState_ld  <= AvlState_l;
    FetchDone_p  <= 1'b0;
    case (AvlState_l)
    s_AvlIdle: begin
        MemWrPointer_c <= 'h0;
        AvlMaster_iot.Read  <= 1'b0;
        AvlMaster_iot.Write <= 1'b0;
    end
    s_AvlPageRead: begin
        FetchRequest <= 1'b0;
        if (AvlState_ld==s_AvlIdle) //first cycle in state
            AvlMaster_iot.Read <= 1'b1;
        else if (~AvlMaster_iot.WaitRequest) //Request accepted
            AvlMaster_iot.Read <= 1'b0;
        if (AvlNextState_l == s_AvlIdle)
            FetchDone_p  <= 1'b1;
        AvlMaster_iot.BurstCount_b <= 2**(g_Log2PageSize-2);
        AvlMaster_iot.ByteEnable   <= 16'hFFFF;
        AvlMaster_iot.Address_b[26-1:(g_Log2PageSize-2)] <= PageToFetch_b;
        AvlMaster_iot.Address_b[(g_Log2PageSize-2)-1:0]  <= 'b0;
        if (AvlMaster_iot.ReadDataValid) begin
            PageMem_mb128[MemWrPointer_c] <= AvlMaster_iot.DataRead_b;
            MemWrPointer_c                <= MemWrPointer_c + 1'b1;
        end
    end
    s_AvlSingleWrite: begin
        if (AvlState_ld==s_AvlIdle) begin//first cycle in state
            ReWriteFifo                 <= 1'b1;
            AvlMaster_iot.Write         <= 1'b1;
            AvlMaster_iot.BurstCount_b  <= 11'd1;
            AvlMaster_iot.Address_b     <= MemoryWriteAddress_b26;
            case (MemoryWriteLwSelect_b2)
                2'd0: begin
                    AvlMaster_iot.DataWrite_b <= {96'h0, MemoryWriteLw_b32};
                    AvlMaster_iot.ByteEnable <= 16'h000F;
                end
                2'd1: begin
                    AvlMaster_iot.DataWrite_b <= {64'h0, MemoryWriteLw_b32, 32'h0};
                    AvlMaster_iot.ByteEnable <= 16'h00F0;
                end
                2'd2: begin
                    AvlMaster_iot.DataWrite_b <= {32'h0, MemoryWriteLw_b32, 64'h0};
                    AvlMaster_iot.ByteEnable <= 16'h0F00;
                end
                2'd3: begin
                    AvlMaster_iot.DataWrite_b <= {MemoryWriteLw_b32, 96'h0};
                    AvlMaster_iot.ByteEnable <= 16'hF000;
                end
            endcase
        end else begin
            ReWriteFifo         <= 1'b0;
            if (~AvlMaster_iot.WaitRequest) AvlMaster_iot.Write <= 1'b0;
        end
    end
    default: begin
        MemWrPointer_c <= 'h0;
        AvlMaster_iot.Read  <= 1'b0;
        AvlMaster_iot.Write <= 1'b0;
    end
    endcase
end

endmodule
